var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var fs = require('fs');
var path = require('path');
var net = require('net');
var express = require('express');
var webServer = express();
var tmpFile = './tmpwifi';
var logFile = './logfile';


var powerOn =  function(on){
  return new Promise((resolve)=>{
    var client = new net.Socket();
    client.on('error', ()=>{})
    client.connect(7681, '192.168.4.1', function() {
      var startCommand = "764c58734a3077776a644a444974356d3659334d4266663234504a61746c4765736d4d7368396632306c413d";
      var stopCommand = "532b37496d375757705457626464424c546a63504371786874756432724636365650585a5854696179616f3d";
      var command = on ? startCommand : stopCommand
      var data = new Buffer(command, "hex")
      client.write(data);
      client.destroy()
      resolve()
    });
  })
}

webServer.use('/public', express.static(path.join(__dirname, 'public')))

webServer.get('/',(req,res)=>{
  res.sendFile(path.join(__dirname + '/index.html'));
})

webServer.get('/on',(req,res)=>{
  powerOn(1).then(()=>{
    res.send('on')
  })
})
webServer.get('/off',(req,res)=>{
  powerOn(0).then(()=>{
    res.send('off')
  })
})
webServer.listen(8081, ()=>{
  console.log('Listening')
})

exec('killall wpa_supplicant')
exec('dhclient -r')
exec('cp /dev/null ' + logFile)
exec('wpa_passphrase HF_SP_C039C5 12345678', (err, stdout)=>{
  fs.writeFileSync(tmpFile, stdout)
  var setWifi = spawn('wpa_supplicant', ['-i', 'wlan0', '-c', tmpFile, '-f', logFile]);
  var logFileStream = spawn('tail', ['-F', logFile]);
  logFileStream.stdout.on('data',(line)=>{
    line = line.toString('utf8');
    if (line.indexOf('WRONG_KEY') > 0){
      logFileStream.kill()
      setWifi.kill();
      console.log('Failed to connect')
    }
    if (line.indexOf('CTRL-EVENT-CONNECTED') > 0){
      logFileStream.kill()
      exec('dhclient wlan0')
      console.log('Connected')
    }
  })
});
